#include <raw_api.h>
#include <stdio.h>
#include <lib_string.h>
#include <rsh.h>
#include <rsh_command.h>

#if (CONFIG_RAW_ISR_STACK_CHECK > 0)

static RAW_S32 rsh_isr_stack_command(RAW_S8 *pcWriteBuffer, size_t xWriteBufferLen, const RAW_S8 *pcCommandString)
{
	RAW_U32 free_isr_stack;
	
	raw_isr_stack_check(&free_isr_stack);

	RAW_PORT_PRINTF("\r\ninterrupt free stack size is %d ", free_isr_stack);
	
	return 1;
}


static xCommandLineInputListItem isr_stack_item ;

static const xCommandLineInput isr_stack_cmd = 
{
	"isr",
	"isr -- show  free interrupt stack size\n",
	rsh_isr_stack_command,
	0
};


void register_isr_stack_command(void)
{
	rsh_register_command(&isr_stack_cmd, &isr_stack_item);
}

#endif

