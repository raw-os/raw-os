#include "hal.h"

#if (HAL_USE_ADC > 0)


HAL_ERROR adc_init(ADC_DRIVER *adc) 
{
	HAL_ERROR ret;
	RAW_OS_ERROR mutex_error;

	RAW_ASSERT(adc != 0);
	
	mutex_error = raw_mutex_get(&adc->adc_lock, RAW_WAIT_FOREVER);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	ret = adc_lld_init(adc);

	mutex_error = raw_mutex_put(&adc->adc_lock);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);

	return ret;
}


HAL_ERROR adc_start(ADC_DRIVER *adc, ADC_CONFIG *config) 
{
	HAL_ERROR ret;
	RAW_OS_ERROR mutex_error;
	
	RAW_ASSERT((adc != 0) && (config != 0));

	mutex_error = raw_mutex_get(&adc->adc_lock, RAW_WAIT_FOREVER);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	adc->config = config;
	ret = adc_lld_start(adc, config);

	mutex_error = raw_mutex_put(&adc->adc_lock);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	return ret;
}


HAL_ERROR adc_stop(ADC_DRIVER *adc) 
{
	HAL_ERROR ret;
	RAW_OS_ERROR mutex_error;
	
	RAW_ASSERT(adc != 0);

	mutex_error = raw_mutex_get(&adc->adc_lock, RAW_WAIT_FOREVER);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	ret = adc_lld_stop(adc);
	
	mutex_error = raw_mutex_put(&adc->adc_lock);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	return ret;
}

HAL_ERROR adc_conversion_start(ADC_DRIVER *adc, ADC_CONVERSION_CONFIG *config, 
									ADC_SAMPLE *samples, RAW_U32 depth) 
{
	HAL_ERROR ret;
	RAW_OS_ERROR mutex_error;
	
	mutex_error = raw_mutex_get(&adc->adc_lock, RAW_WAIT_FOREVER);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	ret = adc_lld_conversion_start(adc, config, samples, depth);
	
	mutex_error = raw_mutex_put(&adc->adc_lock);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	return ret;
  
}


HAL_ERROR adc_conversion_stop(ADC_DRIVER *adc) 
{
	HAL_ERROR ret;
	RAW_OS_ERROR mutex_error;
	
	RAW_ASSERT(adc != 0);

	mutex_error = raw_mutex_get(&adc->adc_lock, RAW_WAIT_FOREVER);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	ret = adc_lld_conversion_stop(adc);
	
	mutex_error = raw_mutex_put(&adc->adc_lock);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	return ret; 
}


HAL_ERROR adc_deinit(ADC_DRIVER *adc)
{
	HAL_ERROR ret;
	RAW_OS_ERROR mutex_error;
	
	RAW_ASSERT(adc != 0);

	mutex_error = raw_mutex_get(&adc->adc_lock, RAW_WAIT_FOREVER);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	ret = adc_lld_deinit(adc);

	mutex_error = raw_mutex_put(&adc->adc_lock);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);

	return ret;
}


#endif 


