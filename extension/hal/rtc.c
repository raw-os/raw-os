#include "hal.h"

#if (HAL_USE_RTC > 0)

HAL_ERROR rtc_init(RTC_DRIVER *rtc)
{
	HAL_ERROR ret;
	RAW_OS_ERROR mutex_error;

	RAW_ASSERT(rtc != 0);
	
	mutex_error = raw_mutex_get(&rtc->rtc_lock, RAW_WAIT_FOREVER);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	ret = rtc_lld_init(rtc);

	mutex_error = raw_mutex_put(&rtc->rtc_lock);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);

	return ret;

}


HAL_ERROR rtc_start(RTC_DRIVER *rtc, RTC_CONFIG *config)
{
	HAL_ERROR ret;
	RAW_OS_ERROR mutex_error;
	
	RAW_ASSERT((rtc != 0) && (config != 0));

	mutex_error = raw_mutex_get(&rtc->rtc_lock, RAW_WAIT_FOREVER);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	rtc->config = config;
	ret = rtc_lld_start(rtc, config);
	
	mutex_error = raw_mutex_put(&rtc->rtc_lock);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	return ret;

}


HAL_ERROR rtc_stop(RTC_DRIVER *rtc)
{

	HAL_ERROR ret;
	RAW_OS_ERROR mutex_error;
	
	RAW_ASSERT((rtc != 0));

	mutex_error = raw_mutex_get(&rtc->rtc_lock, RAW_WAIT_FOREVER);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	ret = rtc_lld_stop(rtc);
	
	mutex_error = raw_mutex_put(&rtc->rtc_lock);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	return ret;

}

HAL_ERROR rtc_time_set(RTC_DRIVER *rtc, RTC_TIME_DATA *time, RAW_U32 format)
{
	HAL_ERROR ret;
	RAW_OS_ERROR mutex_error;
	
	RAW_ASSERT((rtc != 0) && (time != 0) && (format != 0));

	mutex_error = raw_mutex_get(&rtc->rtc_lock, RAW_WAIT_FOREVER);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	ret = rtc_lld_time_set(rtc, time, format);
	
	mutex_error = raw_mutex_put(&rtc->rtc_lock);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	return ret;
}


HAL_ERROR rtc_time_get(RTC_DRIVER *rtc, RTC_TIME_DATA *time, RAW_U32 format)
{

	HAL_ERROR ret;
	RAW_OS_ERROR mutex_error;
	
	RAW_ASSERT((rtc != 0) && (time != 0) && (format != 0));

	mutex_error = raw_mutex_get(&rtc->rtc_lock, RAW_WAIT_FOREVER);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	ret = rtc_lld_time_get(rtc, time, format);
	
	mutex_error = raw_mutex_put(&rtc->rtc_lock);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);

	return ret;

}


HAL_ERROR rtc_deinit(RTC_DRIVER *rtc) 
{
	HAL_ERROR ret;
	RAW_OS_ERROR mutex_error;

	RAW_ASSERT(rtc != 0);
	
	mutex_error = raw_mutex_get(&rtc->rtc_lock, RAW_WAIT_FOREVER);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	ret = rtc_lld_deinit(rtc);

	mutex_error = raw_mutex_put(&rtc->rtc_lock);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);

	return ret;
}



#endif


