#include "hal.h"
#include <raw_api.h>

#if (HAL_USE_WDOG > 0)

HAL_ERROR wdog_init(WDOG_DRIVER *wdog) 
{
	HAL_ERROR ret;
	RAW_OS_ERROR mutex_error;
	
	RAW_ASSERT(wdog != 0);
	
	mutex_error = raw_mutex_get(&wdog->wdog_lock, RAW_WAIT_FOREVER);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);

	ret = wdog_lld_init(wdog);

	mutex_error = raw_mutex_put(&wdog->wdog_lock);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);

	return ret;
	
}


HAL_ERROR wdog_start(WDOG_DRIVER *wdog, WDOG_CONFIG *config)
{
	HAL_ERROR ret;
	RAW_OS_ERROR mutex_error;
	
	RAW_ASSERT((wdog != 0) && (config != 0));

	mutex_error = raw_mutex_get(&wdog->wdog_lock, RAW_WAIT_FOREVER);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	wdog->config = config;
	ret = wdog_lld_start(wdog, config);
	
	mutex_error = raw_mutex_put(&wdog->wdog_lock);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);

	return ret;
}


HAL_ERROR wdog_feed(WDOG_DRIVER *wdog)
{
	HAL_ERROR ret;
	RAW_OS_ERROR mutex_error;
	
	RAW_ASSERT(wdog != 0);

	mutex_error = raw_mutex_get(&wdog->wdog_lock, RAW_WAIT_FOREVER);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	ret = wdog_lld_feed(wdog);
	
	mutex_error = raw_mutex_put(&wdog->wdog_lock);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);

	return ret;
}


HAL_ERROR wdog_stop(WDOG_DRIVER *wdog)
{
	HAL_ERROR ret;
	RAW_OS_ERROR mutex_error;
	
	RAW_ASSERT(wdog != 0);

	mutex_error = raw_mutex_get(&wdog->wdog_lock, RAW_WAIT_FOREVER);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	ret = wdog_lld_stop(wdog);
	
	mutex_error = raw_mutex_put(&wdog->wdog_lock);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);

	return ret;
}


HAL_ERROR wdog_deinit(WDOG_DRIVER *wdog) 
{
	HAL_ERROR ret;
	RAW_OS_ERROR mutex_error;
	
	RAW_ASSERT(wdog != 0);
	
	mutex_error = raw_mutex_get(&wdog->wdog_lock, RAW_WAIT_FOREVER);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);

	ret = wdog_lld_deinit(wdog);

	mutex_error = raw_mutex_put(&wdog->wdog_lock);
	RAW_ASSERT(mutex_error == RAW_SUCCESS);
	
	return ret;
	
}


#endif


