#ifndef UART_H
#define UART_H

#if (HAL_USE_UART > 0)

#include "uart_lld.h"

#ifdef __cplusplus
extern "C" {
#endif

HAL_ERROR uart_init(UART_DRIVER *uart);
HAL_ERROR uart_start(UART_DRIVER *uart, UART_CONFIG *config);
HAL_ERROR uart_stop(UART_DRIVER *uart);
HAL_ERROR uart_transmit(UART_DRIVER *uart, RAW_U8 *tx_buf, RAW_U32 tx_bytes, RAW_U32 timeout);
HAL_ERROR uart_receive(UART_DRIVER *uart, RAW_U8 *rx_buf, RAW_U32 rx_bytes, RAW_U32 timeout);
HAL_ERROR uart_deinit(UART_DRIVER *uart);

#ifdef __cplusplus
}

#endif

#endif

#endif 




