
#ifndef _SPI_H_
#define _SPI_H_

#include "spi_lld.h"

#if (HAL_USE_SPI > 0) 


#ifdef __cplusplus
extern "C" {
#endif

HAL_ERROR spi_init(SPI_DRIVER *spi);
HAL_ERROR spi_start(SPI_DRIVER *spi, SPI_CONFIG *config);
HAL_ERROR spi_stop(SPI_DRIVER *spi);

HAL_ERROR spi_transmit(SPI_DRIVER *spi, RAW_U8 *tx_buf, RAW_U32 tx_bytes, 
								 RAW_U8 *rx_buf, RAW_U32 rx_bytes, RAW_U32 timeout);

HAL_ERROR spi_receive(SPI_DRIVER *spi, RAW_U8 *rx_buf, RAW_U32 rx_bytes, RAW_U32 timeout);


HAL_ERROR spi_deinit(SPI_DRIVER *spi);



  
#ifdef __cplusplus
}
#endif

#endif 

#endif 


