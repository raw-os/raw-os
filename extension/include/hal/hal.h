#ifndef _HAL_H_
#define _HAL_H_

#include "raw_api.h"

#include "halconf.h"


#define HAL_VERSION             "1.0"

#ifdef __cplusplus
extern "C" {
#endif

void hal_init(void);

#ifdef __cplusplus
}
#endif

#endif 

