#ifndef ADC_H
#define ADC_H

#if (HAL_USE_ADC > 0)

#include "adc_lld.h"


#ifdef __cplusplus
extern "C" {
#endif

HAL_ERROR adc_init(ADC_DRIVER *adc);
HAL_ERROR adc_start(ADC_DRIVER *adc, ADC_CONFIG *config);
HAL_ERROR adc_stop(ADC_DRIVER *adc);
HAL_ERROR adc_conversion_start(ADC_DRIVER *adc, ADC_CONVERSION_CONFIG *config, 
									ADC_SAMPLE *samples, RAW_U32 depth);

HAL_ERROR adc_conversion_stop(ADC_DRIVER *adc);
HAL_ERROR adc_deinit(ADC_DRIVER *adc);


#ifdef __cplusplus
}
#endif

#endif 

#endif 


