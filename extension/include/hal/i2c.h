#ifndef _I2C_H_
#define _I2C_H_

#if (HAL_USE_I2C > 0)

#include "i2c_lld.h"

#ifdef __cplusplus
extern "C" {
#endif

HAL_ERROR i2c_init(I2C_DRIVER *i2cp);
HAL_ERROR i2c_start(I2C_DRIVER *i2cp, I2C_CONFIG *config);
HAL_ERROR i2c_stop(I2C_DRIVER *i2cp);
HAL_ERROR i2c_master_transmit(I2C_DRIVER *i2cp,
                             RAW_U32 dev_addr,
                             RAW_U8 *tx_buf, RAW_U32 tx_bytes,
                             RAW_U8 *rx_buf, RAW_U32 rx_bytes,
                             RAW_U32 timeout);

HAL_ERROR i2c_mem_write (I2C_DRIVER *i2cp,
                         RAW_U32 dev_addr, RAW_U16 mem_address, 
                         RAW_U16 mem_size, RAW_U8 *tx_buf, 
                         RAW_U32 tx_bytes, RAW_U8 *rx_buf, 
                         RAW_U32 rx_bytes, RAW_U32 timeout);


HAL_ERROR i2c_master_receive(I2C_DRIVER *i2cp,
                            RAW_U32 dev_addr,
                            RAW_U8 *rx_buf, RAW_U32 rx_bytes,
                            RAW_U32 timeout);

HAL_ERROR i2c_mem_receive(I2C_DRIVER *i2cp,
								RAW_U32 dev_addr, RAW_U16 mem_address, 
								RAW_U16 mem_size, RAW_U8 *rx_buf, 
								RAW_U32 rx_bytes, RAW_U32 timeout);

HAL_ERROR i2c_deinit(I2C_DRIVER *i2cp);




#ifdef __cplusplus
}

#endif

#endif

#endif 

