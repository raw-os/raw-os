#ifndef GPIO_H
#define GPIO_H

#if (HAL_USE_GPIO > 0)

#include "gpio_lld.h"

#ifdef __cplusplus
extern "C" {
#endif

HAL_ERROR gpio_init(GPIO_DRIVER *gpio);
HAL_ERROR gpio_start(GPIO_DRIVER *gpio, GPIO_CONFIG *config);
HAL_ERROR gpio_stop(GPIO_DRIVER *gpio, RAW_U32 gpio_pin);
HAL_ERROR gpio_pin_write(GPIO_DRIVER *gpio, RAW_U32 pin, GPIO_PIN_STATE pin_state);
HAL_ERROR gpio_pin_read(GPIO_DRIVER *gpio, RAW_U32 pin, GPIO_PIN_STATE *pin_value);
HAL_ERROR gpio_deinit(GPIO_DRIVER *gpio);

#ifdef __cplusplus
}


#endif

#endif

#endif
