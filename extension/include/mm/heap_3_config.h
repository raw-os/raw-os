#ifndef HEAP_3_CONFIG_H
#define HEAP_3_CONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

void *mem_3_malloc(size_t xWantedSize);

void mem_3_free(void *pv);

#ifdef __cplusplus
}
#endif


#endif


